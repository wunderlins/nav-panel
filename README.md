# NAV Panel

<img src="assets/icon.png" align="left" width="64" style="padding-right: 1em"> 
This project aims create an auto pilot panel for FS2020. The primary 
goal is to provide generic hardware input buttons and knobs for an 
autopilot (independant of plane type).
<br clear="all">

## Panel layout
A very rough sketch, layout will change.
<img src="assets/sketch.jpg">

## Sub Projects

- [Components](components/README.md)
    - [dual-encoder](components/dual-encoder/README.md)
    - [electronics](components/electronics/README.md)
    - [panel](components/panel/README.md)
- [Device Driver](driver/README.md)
- [SimConnect](simconnect/README.md)