# Dual Rotary Encoder with push Button

[Alsp EC11 Product Listing](https://tech.alpsalpine.com/prod/e/html/encoder/incremental/ec11/ec11_list.html)

The following product has a dual rotary encoder with push button ([Mouser](https://www.mouser.ch/ProductDetail/Alps-Alpine/EC11E183440C?qs=%2Fha2pyFaduh3OJOn4Bj%252B1od6yXssmvmrfC5Nz6LN%252BMjzxbS5TY6qOw%3D%3D)):

| Mouser-Nr. | 688-EC11E183440C |
|------------|------------------|
| Vendor- Nr. | EC11E183440C |
| Vendor. | Alps Alpine |


## Dimensions

![Dimensions](dimensions.png)

![Knobs Render](dual-encoder.png)

- [Step file for knobs](EC11E.stp)
- [Top STL](top-knob.stl), [Bottom STL](bottom-knob.stl) (printed with PETG)
- [Top GCODE](CE5_top-knob.gcode), [Bottom GCODE](CE5_bottom-knob.gcode)
- [Datasheet](alps_alps-s-a0008379064-1-1733314.pdf)